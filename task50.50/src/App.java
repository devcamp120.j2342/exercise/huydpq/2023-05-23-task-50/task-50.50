import java.lang.invoke.MethodHandles.Lookup;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class App {
    public static void main(String[] args) throws Exception {
        // task 1 Đếm số lần xuất hiện của ký tự n trong chuỗi
        String arr1 = "DCresource: JavaScript Exercises";
        char a = 'e';
        int count = 0;
        for (int i = 0; i < arr1.length(); i++) {
            // Nếu ký tự tại vị trí thứ i bằng 'a' thì tăng count lên 1
            if (arr1.charAt(i) == a) {
                count++;
            }
        }
        System.out.println("Task 1: Số lần xuất hiện của ký tự " + a + " trong chuỗi là: " + count);
        // task 2: Loại bỏ các ký tự trắng ở đầu và cuối chuỗi
        String arr2 = "dcresource ";
        System.out.println("Task 2");
        System.out.println(arr2 + "Java");
        System.out.println(arr2.trim() + "Java");

        // task 3: Loại bỏ chuỗi con ra khỏi chuổi cho trước
        String arr3 = "The quick brown fox the jumps over the lazy dog";
        String b = "the";
        String newString = arr3.replace(b, "");
        System.out.println("Task 3: " + newString);

        // task 4: Loại bỏ chuỗi con ra khỏi chuổi cho trước
        String arr4 = "JS PHP PYTHON";
        String c = "PYTHON";

        Boolean x = arr4.endsWith(c);
        System.out.println("Task 4: " + x);

        // task 5:So sánh 2 chuỗi có giống nhau hay không
        String arr5 = "abcd";
        String d = "AbcD";

        System.out.println("Task 5: ");
        System.out.println(arr5.equals(d));// so sánh 2 chuỗi có phân biệt chữ hoa, chữ thường.
        System.out.println(arr5.equalsIgnoreCase(d)); // so sánh 2 chuỗi KHÔNG phân biệt chữ hoa, chữ thường.

        // task 6 Kiểm tra ký tự thứ n của chuỗi có phải viết hoa hay không
        String str6 = "Js STRING EXERCISES";
        int n6 = 0;
        char ch6 = str6.charAt(n6);
        boolean result6 = Character.isUpperCase(ch6);
        System.out.println("Task 6:  Kiem tra ky tu n cua chuoi co viet hoa ko: " + result6);

        // task 7 Kiểm tra ký tự thứ n của chuỗi có phải viết thường hay không
        String str7 = "Js STRING EXERCISES";
        int n7 = 0;
        char ch7 = str7.charAt(n7);
        boolean result7 = Character.isLowerCase(ch7);
        System.out.println("Task 7:  Kiem tra ky tu n cua chuoi co viet thường ko: " + result7);

         // task 8: Kiểm tra chuỗi trước có bắt đầu bằng chuỗi sau hay không
         String str8 = "js string exercises";
         String n8 = "js";
         System.out.println("Task 8: " + str8.startsWith(n8));
         // task 9: Kiểm tra chuỗi đã cho có phải chuỗi rỗng hay không
         String str9 = "abc";
         String str10 = "";
         System.out.println("Task 8: ");
         System.out.println(str9.isEmpty());
         System.out.println(str10.isEmpty());

         // task 10: Đảo nguojc chuỗi

         String str11 = "AaBbc";
         StringBuffer reverse = new StringBuffer(str11);
         System.out.println("Task 10");
         System.out.println(str11);
        System.out.println("Chuỗi sau khi đảo ngược là: " + reverse.reverse().toString());



    }

}